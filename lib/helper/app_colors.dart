import 'package:flutter/material.dart';

class AppColors {
  static const Color white = Color(0xFFffffff);
  static const Color rating_purple = Color(0xFFFFA4CF);
  static const Color purple = Color(0xFF995274);
  static const Color pink = Color(0xFFE15497);
  static const Color dark_pink = Color(0xFF67509E);
  static const Color dark_purple = Color(0xFF34284F);
  static const Color transparent = Colors.transparent;
  static const Color text_font_color = Color(0xFF2E436D);
  static const Color new_blue = Color(0xFF070036);
  static const Color cyan = Color(0xFF7ACCCE);
  static const Color min_grey = Color(0xFFD1D1D1);
  static const Color min_white = Color(0xFFFAFAFA);
  static const Color min_purple = Color(0xFFE15497);
  static const Color f_white = Color(0xFF79859E);
  static const Color f_grey = Color(0xFF96939E);
  static const Color s_white = Color(0xFF9E9E9E);
  static const Color min_blue = Color(0xFF3D637F);
  static const Color fill_grey = Color(0xFFE8E8E8);
  static const Color b_white = Color(0xB3FFFFFF);
  static const Color a_white = Color(0x8AFFFFFF);
  static const Color blue_dark = Color(0xFF112140);
  static const Color c_blue = Color(0xFF24375C);
  static const Color grey = Color(0xFF7F8BA3);
  static const Color dark_blue = Color(0xFF182644);
  static const Color dark_blue_back = Color(0xFF24375D);
  static const Color black = Color(0xFF000000);
  static const Color dark_black = Color(0xFF131019);
  static const Color mainColor = Color(0xFF000000);
  static const Color icon_color = Color(0xFF67509E);
  static const Color MAIN_COLOR = Color(0xFF80C038);
  static const Color Red = Color(0xFFDD4B59);
  static const Color MEATS = Color(0xFFC02828);
  static const Color dark_red = Color(0xFFE80613);
  static const Color FRUITS = Color(0xFFC028BA);
  static const Color VEGS = Color(0xFF28C080);
  static const Color SEEDS = Color(0xFFC05D28);
  static const Color yellow = Color(0xFFD4C520);
  static const Color PASTRIES = Color(0xFF5D28C0);
  static const Color SPICES = Color(0xFF1BB1DE);
  static const Color UnlimitedPlanCard = Color(0xFF6CA5BA);

  // other colors
  static const Color DARK_GREEN = Color(0xFF1B6948);
  static const Color DARKER_GREEN = Color(0xFF0B452C);
  static const Color HIGHTLIGHT_DEFAULT = Color(0xFF5A8E12);
  static const Color LIGHTER_GREEN = Color(0xFFC1E09E);

  static Gradient mainGradient = LinearGradient(
      colors: [AppColors.c_blue, AppColors.dark_blue],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      stops: [0.0, 1.0],
      tileMode: TileMode.clamp);
}
