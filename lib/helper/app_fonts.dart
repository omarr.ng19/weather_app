import 'package:flutter/cupertino.dart';

class AppFonts {
  static const double x_x_small_font_size = 10.0;
  static const double x_small_font_size = 12.0;
  static const double small_font_size = 14.0;
  static const double medium_font_size = 16.0;
  static const double normal_font_size = 18.0;
  static const double large_font_size = 20.0;
  static const double x_large_font_size = 22.0;
  static const double xx_large_font_size = 24.0;
  static const double xxx_large_font_size = 52.0;

  static const FontMontserrat = 'Montserrat';

  static const FontCairo = 'Cairo';

  String getFontFamily(String langCode) {
    if (langCode == 'en') {
      return FontCairo;
    } else if (langCode == 'ar') {
      return FontCairo;
    } else
      return FontCairo;
  }

  static double getScale(langCode, screenWidth, bool withScreenMeasurement) {
    double scale = 1;

    if (withScreenMeasurement) {
      if (screenWidth <= 320) {
        // iPhone 4 & 5 (480 - 568)
        scale = 0.9;
      } else if (screenWidth > 320) {
        // iPhone 6 & 7 (667)
        scale = 0.95;
      } else {
        // iPhone 6+ & 7+ (736)
        scale = 1.0;
      }
    }
    if (langCode != 'ar') {
      scale *= 1;
    } else if (langCode == 'ar') {
      scale *= 0.8;
    } else
      scale = 1;
    return scale;
  }

  static double getXXSmallFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return x_x_small_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getXSmallFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return x_small_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getSmallFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return small_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getMediumFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return medium_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getNormalFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return normal_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getLargeFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return large_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getXLargeFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return x_large_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getXXLargeFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return xx_large_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }

  static double getXXXLargeFontSize(
      {langCode = "en",
      @required screenWidth,
      bool withScreenMeasurement = true}) {
    return xxx_large_font_size *
        getScale(langCode, screenWidth, withScreenMeasurement);
  }
}
