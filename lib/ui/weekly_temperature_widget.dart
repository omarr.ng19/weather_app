

import 'package:flutter/material.dart';
import 'package:weather_app/helper/app_colors.dart';

class WeeklyTemperature extends StatefulWidget {
  final String temperature;
  final String day;
  final String icon;

  const WeeklyTemperature({
    this.temperature,
    this.day,
    this.icon,
  });

  @override
  _WeeklyTemperatureState createState() => _WeeklyTemperatureState();
}

class _WeeklyTemperatureState extends State<WeeklyTemperature> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, top: 10, right: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text(
                widget.day,
                style: TextStyle(fontSize: 18,color: AppColors.white),
              ),
              SizedBox(height:16,)
            ],
          ),
          Row(
            children: [
              Text(
                widget.temperature,
                style: TextStyle(fontSize: 18,color: AppColors.white),
              ),
              SizedBox(
                width: 12,
              ),
              Image.asset(
                "${widget.icon}",
                color: AppColors.white,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
