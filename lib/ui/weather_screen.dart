import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:weather_app/bloc/weather_bloc.dart';
import 'package:weather_app/helper/app_colors.dart';
import 'package:weather_app/helper/app_fonts.dart';
import 'package:weather_app/model/weather_daily_model.dart';
import 'package:weather_app/ui/weekly_temperature_widget.dart';

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  WeatherBloc weatherBloc = WeatherBloc();


  List <String> list = List.generate(47, (index) => "${index%2 == 0 ? 0 : 3}");



  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.new_blue,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height/2.7,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("assets/images/cloud.gif",)),
                  gradient: AppColors.mainGradient,
                  borderRadius: const BorderRadius.only(
                    bottomLeft:  Radius.circular(32),
                    bottomRight:  Radius.circular(32),
                  ),

                ),
                child: StreamBuilder<WeatherDailyModel>(
                  stream: weatherBloc.dailyDataStream,
                  builder: (context, dailyMinSnapshot) {
                    if(dailyMinSnapshot.data !=  null){
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Weather",
                            style: TextStyle(
                                color: AppColors.white,
                                fontSize: AppFonts.getXXLargeFontSize(
                                    screenWidth: MediaQuery.of(context).size.width)),
                          ),
                          const SizedBox(height: 16,),
                          Text(
                            "${dailyMinSnapshot.data.current.temp.toInt()}",
                            style: TextStyle(
                                color: AppColors.yellow,
                                fontSize: AppFonts.getXXXLargeFontSize(
                                    screenWidth: MediaQuery.of(context).size.width)),
                          ),
                          const SizedBox(height: 8,),
                          Text(
                            "${dailyMinSnapshot.data.current.weather[0].description}",
                            style: TextStyle(
                                color: AppColors.yellow,
                                fontSize: AppFonts.getXXLargeFontSize(
                                    screenWidth: MediaQuery.of(context).size.width)),
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          Container(
                            color: AppColors.grey,
                            height: 1,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          StreamBuilder<WeatherDailyModel>(
                              stream: weatherBloc.dailyDataStream,
                              builder: (context, dailyDataSnapshot) {
                                  if(dailyDataSnapshot.data !=  null && dailyDataSnapshot.hasData){
                                    return Container(
                                      height: 80,
                                      child: ListView.builder(
                                          itemCount: dailyDataSnapshot.data.hourly.length,
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context, index) {
                                            return Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: [
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.only(left: 16),
                                                  child: Column(
                                                    children: [
                                                      Image.asset(
                                                        getWeatherIcon("${dailyDataSnapshot.data.hourly[index].weather[0].main}"),
                                                        color: AppColors.white,
                                                      ),
                                                      const SizedBox(
                                                        height: 8,
                                                      ),
                                                      Column(
                                                        children: [
                                                          Text(
                                                            "${dailyDataSnapshot.data.hourly[index].temp.toInt()}",
                                                            style: const TextStyle(
                                                                color: AppColors.white),
                                                          ),
                                                          SizedBox(height: 8,),
                                                          Text("${index%24}"+":"+"${list[index%10]}"+"0",style: TextStyle(color: AppColors.white),),
                                                          const SizedBox(height: 6,),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 8,
                                                )
                                              ],
                                            );
                                          }),
                                    );
                                  }
                                  else{
                                    return Container();
                                  }
                              })
                        ],
                      );
                    }
                else {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [CircularProgressIndicator()]);
                    }
                  }
                ),
              ),
              const SizedBox(height: 16,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                   Text('Weekly', style:  TextStyle(color:  AppColors.white,fontSize: 20),)
                ],
              ),
              StreamBuilder<WeatherDailyModel>(
                  stream: weatherBloc.dailyDataStream,
                  builder: (context, weeklyDataSnapshot) {
                    return ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: weeklyDataSnapshot.data?.daily?.length??0,
                      itemBuilder: (context,index){
                        if(weeklyDataSnapshot.data.daily.isNotEmpty){
                          if(weeklyDataSnapshot.data.daily != null && weeklyDataSnapshot.hasData){
                            return WeeklyTemperature(
                              day: "${getDayName(index)}",
                              icon: getWeatherIcon("${weeklyDataSnapshot.data.daily[index].weather[0].main}"),
                              temperature: "${weeklyDataSnapshot.data.daily[index].temp.eve.toInt()}"+"/"+"${weeklyDataSnapshot.data.daily[index].temp.night.toInt()}",
                            );
                          }
                          else{
                            return Container();
                          }

                        }else{
                          return  const CircularProgressIndicator();
                        }
                    },);
                  })
            ],
          ),
        ),
      ),
    );
  }



  getWeatherIcon(String status){
    switch(status){
      case "Rain":
        return "assets/images/rain-cloud.png";
      case "Clouds":
        return "assets/images/two-clouds.png";
      case "Clear":
        return "assets/images/sun.png";
    }
  }


  getDayName(index){
    switch(index){
      case 0:
        return "Yesterday";
      case 1:
        return "Monday";
      case 2:
        return "Tuesday";
      case 3:
        return "Wednesday";
      case 4:
        return "Thursday";
      case 5:
        return "Friday";
      case 6:
        return "Saturday";
      case 7:
        return "Sunday";
    }
  }

}
