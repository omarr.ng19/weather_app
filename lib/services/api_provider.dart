
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:weather_app/model/weather_daily_model.dart';

class ApiProvider {
  Map<String, String> headers = {
    'Content-Type': 'multipart/form-data',
    'Accept': 'application/json',
    'Accept-Language': 'en'
  };

  static final String _apiBaseUrl = "https://api.openweathermap.org/data/2.5";
  static final String _getWeatherForDay = '$_apiBaseUrl/onecall?lat=33.5102&lon=36.29128&units=metric&exclude=alerts&appid=5f79257ef5b51d64ce7beee30728b3e6';



  Future<WeatherDailyModel> getWeatherOfDay() async {

    var data = await getMultiPartCallApi(_getWeatherForDay);
    try {
      return WeatherDailyModel.fromJson(data);
    } catch (e) {
      print("get weather app error\n\n $e");
      throw data;
    }
  }


  ////////// *****  Main Functions  ***** //////////

  getMultiPartCallApi(url) async {
    Dio dio = Dio();
    var data;
    await dio
        .get(url,
            options: Options(
              headers: headers,
            ))
        .then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
    }).catchError((e) {
      print(e.response.statusCode);
      print(e.response.data);
    });
    return data;
  }
}

final apiProvider = ApiProvider();
