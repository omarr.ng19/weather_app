import 'package:rxdart/rxdart.dart';
import 'package:weather_app/model/weather_daily_model.dart';
import 'package:weather_app/services/api_provider.dart';

class WeatherBloc {
  WeatherBloc() {
    _getWeatherOfDay();
  }

  final _dailyData = BehaviorSubject<WeatherDailyModel>();

  get dailyDataStream => _dailyData.stream;

  _getWeatherOfDay() {
    apiProvider.getWeatherOfDay().then((value) {
      _dailyData.sink.add(value);
    });
  }



}
